class View {
    print_courses(courses) {
        console.log('All courses:')
        for (const course of courses) {
            console.log(course.id + ': ' + course.name)
        }
    }

    print_students(students) {
        console.log('All students:')
        for (const student of students) {
            console.log(student.roll + ': ' + student.name)
        }
    }

    print_enrolled_courses(enrolled_courses) {
        console.log('All Enrolled Courses:')
        for (const course of enrolled_courses) {
            console.log(course.id + ': ' + course.name)
        }
    }

    print_unenrolled_courses(unenrolled_courses) {
        console.log('All Unenrolled Courses:')
        for (const course of unenrolled_courses) {
            console.log(course.id + ': ' + course.name)
        }
    }
}

module.exports = View