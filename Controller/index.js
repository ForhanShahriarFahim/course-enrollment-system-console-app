const prompt = require("prompt-sync")();

const Admin = require('../models/admin.js')
const Student = require('../models/student.js')
const Person = require('../models/person.js')
const View = require('../views/view.js')



anonymous_person = new Person()
view = new View()

anonymous_person.get_operations()
while (true) {
    const opcode = prompt('Enter operation code: ')
    if (opcode == 1) {
        console.log("Please login")
        const username = prompt('Username: ')
        const password = prompt('Password: ')

        let [idx, who] = anonymous_person.intheDb(username, password)
        
        if (idx == -1) {
            console.log("Wrong Credentials")
        } else {
            let isAdmin = false
            let person
            if (who == "admin") {
                admin = new Admin(idx)

                isAdmin = true
                person = admin.get_own_name() + '(admin)'
                console.log("Successfully login as " + admin.get_own_name())
            } else {
                student = new Student(idx)

                console.log("Successfully login as " + student.get_own_name())
                person = student.get_own_name() + '(student)'
            }

            while (true) {
                const op = prompt(person + '>> ')
                if (op == 3) {
                    if (isAdmin) admin = null
                    else student = null
                    console.log("Successfully logout")
                    break
                } else if (op == 4) {
                    if (isAdmin) view.print_courses(admin.get_courses())
                    else view.print_courses(student.get_courses())
                } else if (op == 5) {
                    if (isAdmin) {
                        view.print_students(admin.get_students())
                    } else {
                        console.log("You don't have permission to see")
                    }
                } else if (op == 6 && !isAdmin) {
                    student.delete_account()
                    break
                } else if (op == 7 && !isAdmin) {
                    view.print_enrolled_courses(student.get_enrolled_courses())
                } else if (op == 8 && !isAdmin) {
                    view.print_unenrolled_courses(student.get_unenrolled_courses())
                } else if (op == 9 && !isAdmin) {
                    student.enroll_course()
                }
            }
        }
    } else if (opcode == 2) {
        anonymous_person.get_register()
    }
}
