let database = require('../database/database.json')
const prompt = require("prompt-sync")()
const fs = require("fs");

const Person = require('./person.js')

class Student extends Person {
    constructor(roll) {
        super()
        this.roll = roll
    }

    get_own_name() {
        const student = database.students.filter((one) => {
            return one.roll == this.roll
        })
        return student[0].name
    }

    enroll_course() {
        const course_id = prompt('Course id: ')

        //console.log("Problem here")
        let data = fs.readFileSync('../database/database.json')
        let object = JSON.parse(data)
        
        
        for (const student of object.students) {
            if (student.roll == this.roll) {
                student.enrolled.push(Number(course_id)) 

            }
        }
        let new_data = JSON.stringify(object)
        try {
            fs.writeFileSync('database', new_data)
            console.log("Successfully enrolled")
        } catch (err) {
            console.error(err)
        }
    }

    get_enrolled_courses() {
        const enrolled_courses = []
        
        const student = database.students.filter((one) => {
            return one.roll == this.roll
        })
        student[0].enrolled.filter((id) => {
            database.courses.filter((course) => {
                if (course.id == id) {
                    enrolled_courses.push(course)
                }
            })
        })
        return enrolled_courses
    }

    get_unenrolled_courses() {
        const unenrolled_courses = []
        const student = database.students.filter((one) => {
            return one.roll == this.roll
        })
        student[0].unenrolled.filter((id) => {
            database.courses.filter((course) => {
                if (course.id == id) {
                    unenrolled_courses.push(course)
                }
            })
        })
        return unenrolled_courses
    }

    delete_account() {
        let data = fs.readFileSync('database.json')
        let object = JSON.parse(data)
        object.students = object.students.filter((student) => {
            return student.roll != this.roll
        })
        
        let new_data = JSON.stringify(object)
        try {
            fs.writeFileSync('database.json', new_data)
            console.log("Successfully deleted your account !!")
        } catch (err) {
            console.error(err)
        }
    }
}

module.exports = Student