let database = require('../database/database.json')
const prompt = require("prompt-sync")()
const fs = require("fs");

const Person = require('./person.js')

class Admin extends Person {
    constructor(id) {
        super()
        this.id = id
    }

    get_own_name() {
        const admin = database.admins.filter((one) => {
            return one.id == this.id
        })
        return admin[0].name
    }

    get_students() {
        const students = []
        for (const student of database.students) {
            students.push(student)
        }
        return students
    }
}

module.exports = Admin