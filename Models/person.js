let database = require('../database/database.json')
const prompt = require("prompt-sync")()
const fs = require("fs");

class Person {
    get_courses() {
        const courses = []
        for (const course of database.courses)      {
            courses.push(course)
        }
        return courses
    }
    
    get_register() {
        console.log("register->")
        const name = prompt('Name: ')
        const roll = prompt('Roll: ')
        const username = prompt('Username: ')
        const password = prompt('Password: ')
        let student = {
            "name": name,
            "roll": Number(roll),
            "username": username,
            "password": password,
            "enrolled": [],
            "unenrolled": []
        }

        let data = fs.readFileSync('database.json')
        let object = JSON.parse(data)
        object.students.push(student)
        let new_data = JSON.stringify(object)
        try {
            fs.writeFileSync('database.json', new_data)
            console.log("Successfully Register")
        } catch (err) {
            console.error(err)
        }
    }

    get_operations() {
        for (const op of database.operations) {
            console.log(op)
        }
    }

    intheDb(username, password) {
        for (let index = 0; index < database.admins.length; index++) {
            if (database.admins[index].username == username && database.admins[index].password == password) {
                return [database.admins[index].id, "admin"]
            }
        }
        for (let index = 0; index < database.students.length; index++) {
            if (database.students[index].username == username && database.students[index].password == password) {
                return [database.students[index].roll, "student"]
            }
        }
        return [-1, "null"]
    }
}

module.exports = Person